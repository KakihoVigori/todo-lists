import Vue from 'vue'
import VueRouter from 'vue-router'
import Note from '../views/Note.vue'
import Notes from '../views/Notes.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Notes',
		component: Notes,
		meta: { title: "Списки дел" }
	},
	{
		path: '/notes/add',
		name: 'NotesAdd',
		component: Note,
		meta: { title: "Создание списка" }
	},
	{
		path: '/notes/:objectID',
		name: 'NoteEdit',
		component: Note,
		meta: { title: "Редактирование списка" }
	}
]

const router = new VueRouter({
	mode: 'history',
	routes
})

router.beforeEach((to, from, next) => {
	document.title = to.meta.title || "Списки";
	next();
});

export default router
