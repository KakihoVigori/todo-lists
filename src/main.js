import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import "./store"
import "./assets/less/main.less"

Vue.config.productionTip = false

//при нажатии на TAB включаем подсветку тех элементов, где сейчас фокус. main.less
//для тех, кому надо или просто хочется перемещаться табом
document.addEventListener('keydown', event => {
	if (event.key == "Tab") {
		document.body.classList.add("tabNavigation_active")
	}
});

new Vue({
	store,
	router,
	render: function (h) { return h(App) }
}).$mount('#app')



/*
	Модальные окна инициализируются в ./App.vue
	Каждое модальное окно должно использовать миксины ./mixins/modal_base.js
	Все модальные окна лежат в ./modals
*/

/*
	Сохранение заметок реализовано через vuex и плагином к нему vuex-persistedstate (сохранение в LS)
	Так же реализована синхронизация данных с разных вкладок\окон браузеров в ./store/index.js
*/
