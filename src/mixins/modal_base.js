import createFocusTrap from 'focus-trap';
export default {
	data() {
		return {
			isShow: false
		}
	},
	mounted() {

		this.modal_effects = {
			show: "fadeIn",
			hide: "fadeOut"
		}
		this.$refs.modal_root.classList.add("animated")
		this.$refs.modal_root_face.classList.add("animated")
	},
	methods: {
		setEffects({ show = "fadeIn", hide = "fadeOut" }) {
			this.modal_effects = {
				show, hide
			}
			return this
		},
		show() {
			this.focusTrap = createFocusTrap(this.$refs.modal_root_face);

			this.isShow = true
			this.$refs.modal_root.classList.add("show", "fadeIn")
			this.$refs.modal_root_face.classList.add(this.modal_effects.show)
			this.$nextTick(() => {
				console.log("asd")
				this.focusTrap.activate()
			})
			setTimeout(() => {
				this.$refs.modal_root_face.classList.remove(this.modal_effects.show)
				this.$refs.modal_root.classList.remove("fadeIn")
			}, 300);

			this.$emit("show")
			return this;
		},
		hide() {
			this.focusTrap.deactivate()
			this.isShow = false
			this.$refs.modal_root_face.classList.add(this.modal_effects.hide)
			this.$refs.modal_root.classList.add("fadeOut")
			setTimeout(() => {
				this.$refs.modal_root_face.classList.remove(this.modal_effects.hide)
				this.$refs.modal_root.classList.remove("fadeOut", "show")
				this.$emit("hide")
			}, 300);

			return this;
		}
	}
}