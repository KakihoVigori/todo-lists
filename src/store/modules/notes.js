import Vue from 'vue'
import ObjectID from "../../assets/js/objectID.js"
const state = {
	all: {}
}

const getters = {
	getAll: state => {
		return JSON.parse(JSON.stringify(state.all));
	},
	getOne: state => objectID => {
		if (state.all[objectID] == undefined) return false
		return JSON.parse(JSON.stringify(state.all[objectID]));
	}
}


const mutations = {
	add(state, { data, cb = false }) {
		let objectID = ObjectID().toString();
		data = JSON.parse(JSON.stringify(data))

		Vue.set(state.all, objectID, Object.assign(data, { objectID }))
		if (cb) {
			cb(objectID)
		}
	},

	update(state, data) {
		if (!(data.objectID in state.all)) return
		data = JSON.parse(JSON.stringify(data))
		Vue.set(state.all, data.objectID, data)
	},

	remove(state, objectID) {
		if (!(objectID in state.all)) return
		Vue.delete(state.all, objectID)
	},

	removeAll(state) {
		state.all = {}
	}
}


export default {
	namespaced: true,
	state,
	getters,
	mutations
}