import Vue from 'vue'
import Vuex from 'vuex'
import notes from './modules/notes'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		notes
	},
	plugins: [createPersistedState()]
})

//Синхронизация Vuex(store) в разных вкладках/окнах браузера 
window.addEventListener('storage', event => {
	if (event.newValue == null || event.newValue == "1") return
	store.replaceState(JSON.parse(event.newValue));
})
export default store